`use strict`
const { validate } = require(`${__basedir}/app/helper/helper`)
module.exports = async function authorizeSvc (req, res, next) {
    let authorize = await db.application.findOne({ 
        where: { secret: req.headers.authorization }
    })
    await validate(res, authorize, 'Unauthorized', 403)
    next()
}