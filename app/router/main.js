'use strict'
const authorize = require(`${__basedir}/app/middleware/authorize`)
const authorizeSvc = require(`${__basedir}/app/middleware/authorize-svc`)

module.exports = (router) => {
    router.get('/display-picture', 'display-picture', [authorize], (req, res) => {
        models.main.displayPicture(req, res)
    })
    router.post('/save-display-picture', 'admin-display-picture', [authorizeSvc], (req, res) => {
        models.main.saveDisplayPicture(req, res)
    })
    router.get('/display-picture/:id', 'admin-display-picture', [authorize], (req, res) => {
        models.main.adminDisplayPicture(req, res)
    })
}