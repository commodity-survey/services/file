`use strict`
const { Validator: v } = require('node-input-validator')
const { validate } = require(`${__basedir}/app/helper/helper`)
const fs = require('fs')

module.exports = {
    displayPicture: async (req, res) => {
        let account = await db.s_personal_data.findOne({
            attributes: [ 'photo' ],
            where: { appid: req.auth.app.id, username: req.auth.username, statusid: 1 },
            raw: true
        })
        res.sendFile(`${__basedir}/protected/account/${account.photo}`)
    },
    saveDisplayPicture: async (req, res) => {
        let validator = new v( { ...req.files, ...req.body }, {
            filename: 'required',
            picture: 'required',
            'picture.mimetype': 'in:image/jpg,image/jpeg,image/png'
        })
        await validate(res, await validator.check(), validator.errors)

        fs.rename(req.files.picture.file, `${__basedir}/protected/account/${req.body.filename}`, async (err) => {
            await validate(res, !err, 'Failed to save display picture')
            res.json({ status: 1 })
        })
    },
    adminDisplayPicture: async (req, res) => {
        let validator = new v(req.query, { id: 'required' })
        await validate(res, await validator.check(), validator.errors)

        let account = await db.s_personal_data.findOne({
            attributes: [ 'photo' ],
            where: { appid: req.auth.app.id, username: req.query.id, statusid: 1 },
            raw: true
        })
        res.sendFile(`${__basedir}/protected/account/${account.photo}`)
    },
}